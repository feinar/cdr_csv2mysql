# cdr_csv2mysql #
---------------

This is a fork of http://kaneda.bohater.net/files/asterisk_csv2mysql_converter.sh updated for my csv file structure and table cdr.

This script will insert data from asterisk cdr Master.csv file to mysql database.

## Usage ##
---------------

Add your database configuration in file `db.config`



To run script:

```
#!shell

$ ./cdr_csv2mysql.sh Master.csv
```

**********


To use cdr_csv2mysql_comparsion.sh:

* ```chmod 755 cdr_csv2mysql_comparsion.sh```
* remove unnecessary lines in Master.csv:

```
#!shell

sed '1,604116d' Master.csv > Master1.csv
```

This removes the interval between lines 1 and 604116

* Rename Master1.csv to Master.csv and put it in cdr_csv2mysql folder
* Run script

```
#!shell

$ ./cdr_csv2mysql_comparsion.sh Master.csv
```

  